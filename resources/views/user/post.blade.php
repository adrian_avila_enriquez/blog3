@extends('layouts.user')

@section('content')
<!-- Post Title-->
<div class="row">
  <br>
  <div class="col-lg-12" align="left">
    <!-- Title -->
    <h1 class="page-header"> {!! $post->title !!}</h1>
  </div>
</div>
<!-- Post Body -->
<div class="row">
	<!-- Blog Post Content Column -->
	<div class="col-lg-8">
		<!-- Author -->
		<p class="lead">
			by {!! $post->user->username !!}
		</p>
		<!-- Date/Time -->
		<p><span class="glyphicon glyphicon-time"></span> Posted on {!! $post->created_at !!}</p>
		<hr>
		<!-- Post Image --> 
   @if ( $post->picture_id != 'null' )
   <!-- <img id="post_picture" class="img-responsive" src="{{ Cloudy::show( $post->picture_id,  array('width' => 900, 'height' => 300, 'crop' => 'fill', 'radius' => 10)) }}" /> -->
   <!-- <img id="post_picture" class="img-responsive" src="{{ Cloudy::show( 'laravel-900x300_yfmaf5',  array('width' => 900, 'height' => 300, 'crop' => 'fill', 'radius' => 10)) }}" /> -->
   @else
   @endif
   <hr>
   <!-- Post Content -->
   <p class="lead">{!! $post->content !!}</p>
 </div>
 <!-- Blog Sidebar Widgets Column -->
 <div class="col-md-4">
   <!-- Word Search Well -->
   <div class="well">
    <h4>Word Search</h4>
    <div class="input-group">
      <input type="text" class="form-control">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button">
          <span class="glyphicon glyphicon-search"></span>
        </button>
      </span>
    </div>
  </div>
  <!-- Blog Categories -->
  <div class="well">
    <h4>Post Categories</h4>
    <div class="row">
      <div class="col-lg-6">
        <ul class="list-unstyled">
         @foreach($post->categories as $category)
         -{!! $category->name !!}
         @endforeach
       </ul>
     </div>
   </div>
 </div>
 <!-- Blog Tags -->
 <div class="well">
  <h4>Post Tags</h4>
  <div class="row">
    <div class="col-lg-6">
      <ul class="list-unstyled">
       @foreach($post->tags as $tag)
       -{!! $tag->description !!}
       @endforeach
     </ul>
   </div>
 </div>
</div>

</div>
</div>


<div id="disqus_thread"></div>
<script type="text/javascript">

  var disqus_shortname = 'avila92';

  (function() {
    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
  })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

<script type="text/javascript">

  var disqus_shortname = 'avila92';

  (function () {
    var s = document.createElement('script'); s.async = true;
    s.type = 'text/javascript';
    s.src = '//' + disqus_shortname + '.disqus.com/count.js';
    (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
  }());
</script>
@stop

