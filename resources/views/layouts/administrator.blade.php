@extends('layouts.master')

@section('navbar')
@parent
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			{!! HTML::linkRoute('administrator.index', 'Blog', array(), array('class' => 'navbar-brand')) !!}
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li>
					{!! HTML::linkRoute('user.index', 'Users') !!}
				</li>
				<li>
					{!! HTML::linkRoute('category.index', 'Categories') !!}
				</li>
				<li>
					{!! HTML::linkRoute('tag.index', 'Tags') !!}
				</li>
				<li>
					{!! HTML::linkRoute('login.index', 'Log out') !!}
				</li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>
@stop
