<html lang="en">
<head>
	@section('head')
	<!--Head-->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/css/select2.min.css" rel="stylesheet" /> 	
	@show
</head>

<body>

	@section('errors')
	<!-- Errors -->
	@if ($errors->any())
	<br><br>
	<div class="alert alert-dismissible alert-danger">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Operation failed!</strong>
		<p> Next problems were found:</p>
		<ul>
			{!! implode('', $errors->all('<li>:message</li>')) !!}
		</ul>  
	</div>	        
	@endif
	@show

	@section('message')
	<!-- Message-->
	@if (Session::has('message'))
	<br><br>		
	<div class="alert alert-dismissible alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>{!! Session::get('message') !!}</strong>
	</div>
	@endif
	@show

	@section('navbar')
	<!-- Navigation bar-->
	@show

	<!-- Page Content-->
	<div class="container">
		@yield('content')
	</div>

	@section('footer')
	<!-- Footer -->
	<div align="center">
		<footer>
			<div class="row">
				<div class="col-lg-12">
					<p>Copyright &copy; Azur Blog 2015</p>
				</div>
			</div>
			<!-- /.row -->
		</footer>
	</div>
	@show
	
	@section('script')
	<!--Script-->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>
	<script type="text/javascript"> $('select').select2() </script>
	@show   
	
</body>
</html>
