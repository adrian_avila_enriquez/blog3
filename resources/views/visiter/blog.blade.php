@extends('layouts.visiter')

@section('content')
<!-- Welcome Message -->
<div class="row">
	<div class="col-lg-12" align="right">
		<h1 class="page-header">Azur Blog
			<small>Welcome visiter!</small>
		</h1>
	</div>
</div>
<!-- New entry-->
<div class="jumbotron">
	{!! Form::open(array('route' => array('post.store'),  'files' => true)) !!}
	<fieldset>
		<div class="form-group">
			<label for="title" class="col-lg-2 control-label">Title:</label>
			<div class="col-lg-10">
				<input type="text" name="title" class="form-control" id="title" value="{{$title}}" placeholder="This is my new post">
			</div>
		</div>
		<div class="form-group">
			<label for="content" class="col-lg-2 control-label" >Content:</label>
			<div class="col-lg-10">
				<textarea class="form-control" name="content" rows="3" id="textArea" value="{{$content}}" placeholder="About Azurlingua..."></textarea>
				<span class="help-block">Write a cool post!</span>
			</div>
		</div>
		<div class="form-group">
			<label for="select" class="col-lg-2 control-label">Categories:</label>
			<div class="col-lg-10">
				{!! Form::select('categories[]',$category_options,null,array('multiple'=>'multiple', 'class'=>'form-control')) !!}
				<span class="help-block">Select as many as you want!</span>
			</div>
		</div>
		<div class="form-group">
			<label for="picture" class="col-lg-2 control-label">Picture:</label>
			<div class="col-lg-10">	
				<div style="text-align:left;" class="content-box">
					<div id="picbox" class="clear" style="padding-top:0px;padding-bottom:10px;">
						<br/ >
						<img id="uploadThumbnail" src="http://placehold.it/200x150" />	
					</div>
					<div class="clear">
						<button id="upload-btn" class="btn btn-primary btn-large clearfix"><i class="fa fa-cloud-upload"></i> Select file</button>
						<br />
						<div id="errormsg" class="clearfix redtext"><small>(JPG, PNG, GIF)</small></div>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group" align="right">
			<div class="col-lg-10 col-lg-offset-2">
				<br>
				{!! Form::reset('Cancel',  array('class' => 'btn btn-danger')) !!}
				{!! Form::submit('Done',  array('class' => 'btn btn-success')) !!}
			</div>
		</div>

	</fieldset>
	{!! Form::close() !!}
</div>
<!-- Posts -->
@foreach($posts as $post)
<div class="row">
	<div class="col-md-7">
		<div class="clear" style="padding-top:0px;padding-bottom:10px;">
			<a href="{!! route('post.show', array('post' => $post->id) ) !!}">
				@if ( $post->picture_id != 'null' ) 
					<img id="post_picture" class="img-responsive" src="" />
					<!-- {!! Cloudy::show( $post->picture_id,  array('width' => 900, 'height' => 300, 'crop' => 'fill', 'radius' => 10)) !!} -->
					<!-- {!! Cloudy::show( 'laravel-900x300_yfmaf5',  array('width' => 900, 'height' => 300, 'crop' => 'fill', 'radius' => 10)) !!} -->
				@else
					<img id="post_picture" class="img-responsive" src="" />
				@endif
			</a>
		</div>
	</div>
	<div class="col-md-5">
		<h2>{!! $post->title !!}</h2>
		<h4>[By {!! $post->user->username !!}]</h4>
		<h5>Posted on {!! $post->created_at !!}</h5>
		<p>Categories :
			@foreach($post->categories as $category)
				{!! $category->name !!}
			@endforeach
		</p>
		<p>Tagged as :
			@foreach($post->tags as $tag)
				{!! $tag->description !!}
			@endforeach
		</p>
	</div>
</div>
@endforeach 
@stop

