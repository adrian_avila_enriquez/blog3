@extends('layouts.master')

@section('head')
@parent
@stop

@section('errors')
@parent
@stop

@section('message')
@parent
@stop

@section('content')	
<!-- Login module -->	
<div id="loginModal" class="container" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">				
				<h1 class="text-center">Login</h1>
			</div>
			<div class="modal-body">
				{!! Form::open(array('route' => array('login.store'), 'class' => 'form col-md-12 center-block')) !!}
				<div class="form-group">
					<label type="text">E-mail:</label>
					<input type="text" name="email" class="form-control input-lg" placeholder="jhon.smith@gmail.com">
				</div>
				<div class="form-group">
					<label type="text">Password:</label>
					<input type="password" name="password" class="form-control input-lg" placeholder="********">
				</div>
				<div class="form-group">
					{!! Form::submit('Sign in',  array('class' => 'btn btn-primary btn-lg btn-block')) !!}
					<span class="pull-right">{!! HTML::linkRoute('signup', 'Register') !!}</span>
				</div>
				{!! Form::close() !!}
			</div>
			<div class="modal-footer">
				<div class="col-md-12">		
					{!! HTML::linkRoute('post.index', 'Back to blog', array(), array('class' => 'btn btn-primary')) !!}
				</div>	
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
@parent
@stop

@section('script')
@parent
@stop



