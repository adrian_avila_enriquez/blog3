@extends('layouts.administrator')

@section('content')            
<div class="row">
  <br>
  <div class="col-lg-12" align="right">
    <h1 class="page-header"> Administrate:
      <small>tags</small>
    </h1>
  </div>
</div>

<div class="jumbotron" align="center">    
  <table class="table table-striped table-hover ">
    <thead>
      <tr>
        <th>Tag Id:</th>
        <th>Description:</th>
        <th>Created on:</th>
        <th></th>
      </tr>
    </thead>
    <tbody>     
      @foreach($tags as $tag)
      <tr>
        {!! Form::open(array('route' => array('tag.destroy', $tag->id), 'method' => 'DELETE')) !!}
        <td>{!! $tag->id !!}</td>
        <td>{!! $tag->description !!}</td>
        <td>{!! $tag->created_at !!}</td>
        <td>{!! Form::submit('X', array('class' => 'btn btn-danger', 'onClick' => "return confirm('Are you sure you want to delete this tag ?')")) !!}</td>
        {!! Form::close() !!} 
      </tr>
      @endforeach     
    </tbody>
  </table>  
</div>

<div class="row">
  <br>
  <div class="col-lg-12" align="right">
    <small>Add new tag?</small>
  </div>
</div>

<div class="jumbotron">
  {!! Form::open(array('route' => array('tag.store'))) !!}
  <fieldset>
    <div class="form-group">
      <label for="tag_description" class="col-lg-2 control-label">Description:</label>
      <div class="col-lg-10">
        <input type="text" name="tag_description" class="form-control" id="tag_description" placeholder="Cool tag">
      </div>
    </div>
    <div class="form-group" align="right">
      <div class="col-lg-10 col-lg-offset-2">
        <br>
        {!! Form::reset('Cancel',  array('class' => 'btn btn-default')) !!}
        {!! Form::submit('Add',  array('class' => 'btn btn-success')) !!}
      </div>
    </div>
  </fieldset>
  {!! Form::close() !!} 
</div>
<hr>
@stop
