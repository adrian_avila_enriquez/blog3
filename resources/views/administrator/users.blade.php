@extends('layouts.administrator')

@section('content')
<div class="row">
	<br>
	<div class="col-lg-12" align="right">
		<h1 class="page-header"> Administrate:
			<small>users</small>
		</h1>
	</div>
</div>
<div class="jumbotron" align="center">		
	<table class="table table-striped table-hover ">
		<thead>
			<tr>
				<th>User Id:</th>
				<th>Name:</th>
				<th>Userame:</th>
				<th>Email:</th>
				<th>Registered on:</th>
				<th></th>
			</tr>
		</thead>
		<tbody>			
			@foreach($users as $user)
			<tr>
				{!! Form::open(array('route' => array('user.destroy', $user->id), 'method' => 'DELETE')) !!}
				<td>{!! $user->id !!}</td>
				<td>{!! $user->name !!}</td>
				<td>{!! $user->username !!}</td>
				<td>{!! $user->email !!}</td>
				<td>{!! $user->created_at !!}</td>
				<td>{!! Form::submit('X', array('class' => 'btn btn-danger', 'onClick' => "return confirm('Are you sure you want to delete this user ?')")) !!}</td>
				{!! Form::close() !!} 
			</tr>
			@endforeach 		
		</tbody>
	</table> 	
</div>
<hr>
@stop

