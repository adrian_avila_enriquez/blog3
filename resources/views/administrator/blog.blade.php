@extends('layouts.administrator')

@section('content')
<!-- Welcome Message -->
<div class="row">
	<br>
	<div class="col-lg-12" align="right">
		<h1 class="page-header">{!! $user->name !!}
			<small>administrator</small>
		</h1>
	</div>
</div>
<!-- Posts -->
@foreach($posts as $post)
<div class="row">
	<div class="col-md-7">
		<div id="picbox" class="clear" style="padding-top:0px;padding-bottom:10px;">
			<a href="{{route('post.show', array('post' => $post->id) )}}">
				@if ( $post->picture_id != 'null' )
					<img id="post_picture" class="img-responsive" src="" />
					<!-- <img id="post_picture" class="img-responsive" src="{{ Cloudy::show( $post->picture_id,  array('width' => 900, 'height' => 300, 'crop' => 'fill', 'radius' => 10)) }}" /> -->
					<!-- <img id="post_picture" class="img-responsive" src="{{ Cloudy::show( 'laravel-900x300_yfmaf5',  array('width' => 900, 'height' => 300, 'crop' => 'fill', 'radius' => 10)) }}" /> -->
				@else
					<img id="post_picture" class="img-responsive" src="" />
				@endif
			</a>
		</div>
		<div>
			<p>
				{!! Form::open(array('route' => array('post.update', $post->id), 'method' => 'PUT')) !!}
				{!! Form::submit('Add tag?',  array('class' => 'btn btn-warning btn-sm')) !!}		
				{!! Form::select('tags[]',$tag_options,null,array('multiple'=>'multiple')) !!} 			
				{!! Form::close() !!}
			</p>
		</div>
	</div>
	<div class="col-md-5">
		<h2>{!! $post->title !!}</h2>
		<h4>By {!! $post->user->username !!}</h4>
		<h5>Posted on {!! $post->created_at !!}</h5>
		<p>Categories :
			@foreach($post->categories as $category)
			{!! $category->name !!}
			@endforeach
		</p>
		<p>Tagged as :
			@foreach($post->tags as $tag)
			{!! $tag->description !!}
			@endforeach
		</p>		
		<p>
			{!! Form::open(array('route' => array('post.destroy', $post->id), 'method' => 'DELETE')) !!}
			{!! Form::submit('Delete', array('class' => 'btn btn-danger ', 'onClick' => "return confirm('Are you sure you want to delete this post ?')")) !!}
			{!! Form::close() !!}
		</p>
	</div>
</div>
<hr>
@endforeach 
@stop

