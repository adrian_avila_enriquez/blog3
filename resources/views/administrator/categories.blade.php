@extends('layouts.administrator')

@section('content')    
<div class="row">
  <br>
  <div class="col-lg-12" align="right">
    <h1 class="page-header"> Administrate:
      <small>categories</small>
    </h1>
  </div>
</div>
<div class="jumbotron" align="center">    
  <table class="table table-striped table-hover ">
    <thead>
      <tr>
        <th>Category Id:</th>
        <th>Name:</th>
        <th>Created on:</th>
        <th></th>
      </tr>
    </thead>
    <tbody>     
      @foreach($categories as $category)
      <tr>
        {!! Form::open(array('route' => array('category.destroy', $category->id), 'method' => 'DELETE')) !!}
        <td>{!! $category->id !!}</td>
        <td>{!! $category->name !!}</td>
        <td>{!! $category->created_at !!}</td>
        <td>{!! Form::submit('X', array('class' => 'btn btn-danger', 'onClick' => "return confirm('Are you sure you want to delete this category ?')")) !!}
          {!! Form::close() !!} 
        </tr>
        @endforeach     
      </tbody>
    </table>  
  </div>

  <div class="row">
    <br>
    <div class="col-lg-12" align="right">
      <small>Add new category?</small>
    </div>
  </div>

  <div class="jumbotron">
    {!! Form::open(array('route' => array('category.store'))) !!}
    <fieldset>
      <div class="form-group">
        <label for="category_name" class="col-lg-2 control-label">Name:</label>
        <div class="col-lg-10">
          <input type="text" name="category_name" class="form-control" id="category_name" placeholder="Cool category">
        </div>
      </div>
      <div class="form-group" align="right">
        <div class="col-lg-10 col-lg-offset-2">
          <br>
          {!! Form::reset('Cancel',  array('class' => 'btn btn-default')) !!}
          {!! Form::submit('Add',  array('class' => 'btn btn-success')) !!}
        </div>
      </div>
    </fieldset>
    {!! Form::close() !!} 
  </div>
  <hr>
  @stop
