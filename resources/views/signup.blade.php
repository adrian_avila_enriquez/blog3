@extends('layouts.master')

@section('head')
@parent
@stop

@section('errors')
@parent
@stop

@section('content')
<!-- Sign Up module -->   
<div id="loginModal" class="container" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
            <div class="modal-content">
                  <div class="modal-header">                      
                        <h1 class="text-center">Sign Up</h1>
                  </div>
                  <div class="modal-body">
                        {!! Form::open(array('route' => array('signup'), 'class' => 'form col-md-12 center-block')) !!}
                        <div class="form-group">
                              <label type="text">Name:</label>
                              <input type="text" name="name" class="form-control input-lg" value="{{$name}}" placeholder="John Smith">
                        </div>
                        <div class="form-group">
                              <label type="text">Username:</label>
                              <input type="text" name="username" class="form-control input-lg" value="{{$username}}" placeholder="Smith87">
                        </div>
                        <div class="form-group">
                              <label type="text">E-mail:</label>
                              <input type="text" name="email" class="form-control input-lg" value="{{$email}}" placeholder="jsmith87@gmail.com">
                        </div>
                        <div class="form-group">
                              <label type="text">Password:</label>
                              <input type="password" name="password" class="form-control input-lg" placeholder="********">
                        </div>
                        <div class="form-group">
                              <label type="text">Password confirmation:</label>
                              <input type="password" name="password_confirmation" class="form-control input-lg" placeholder="********">
                        </div>
                        <div class="form-group">
                              {!! Form::submit('Done!',  array('class' => 'btn btn-primary btn-lg btn-block')) !!}
                        </div>
                        {!! Form::close() !!}
                  </div>

                  <div class="modal-footer">
                        <div class="col-md-12">       
                              <span class="pull-right">{!! HTML::linkRoute('post.index', 'Forget it') !!}</span>
                        </div>      
                  </div>
            </div>
      </div>
</div>
@stop

@section('footer')
@parent
@stop

@section('script')
@stop
