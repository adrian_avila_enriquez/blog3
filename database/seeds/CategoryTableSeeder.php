<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('categories')->delete();
        \DB::table('categories')->insert(array(
          array(
            'name'   => 'Security',
            ),
          array(
            'name'   => 'Videogames',
            ),
          array(
            'name'   => 'Semantics',
            ),
          array(
            'name'   => 'Design',
            )
          ));
    }
}