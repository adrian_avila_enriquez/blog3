<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PostTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('posts')->delete();
        \DB::table('posts')->insert(array(
          array(
            'user_id'       => '4',
            'title'         => 'First post',
            'content'       => 'This is the first post: Nothing',
            'picture_id'    => 'null'
            ),
          array(
            'user_id'       => '3',
            'title'         => 'Second post',
            'content'       => 'This is the second post: Still nothing',
            'picture_id'    => 'null'
            ),
          array(
            'user_id'       => '2',
            'title'         => 'Third post',
            'content'       => 'This is the third post: Something',
            'picture_id'    => 'null'
            )
          ));
    }
}