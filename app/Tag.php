<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model {

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    protected $fillable = array('description'); 

    protected $dates = ['deleted_at'];

    public function posts(){

        return $this->belongsToMany('App\Post', 'post_tag', 'tag_id', 'post_id');
    }
	

}
