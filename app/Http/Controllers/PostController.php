<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use App\User;
use App\Post;
use App\Category;
use App\Tag;
use Auth;

class PostController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Displays all posts
		$category_options = Category::lists('name', 'id');
		$tag_options = Tag::lists('description', 'id');
		$posts = Post::orderBy('created_at', 'DESC')->with('user', 'categories', 'tags')->get();	
		$cors_location;

		if (Auth::check()){

			$user = Auth::user();

			if($user->rol == 0){
				// User
				return view('user.blog')
				->with('user', $user)
				->with('category_options', $category_options)
				->with('tag_options', $tag_options)
				->with('posts', $posts)
				->with('title', '')
				->with('content', '');
			}
			else{
				// Administrator
				return redirect()->route('administrator.index');
			}		
		}
		else{
			// Visiter
			return view('visiter.blog')
			->with('category_options', $category_options)
			->with('tag_options', $tag_options)
			->with('posts', $posts)
			->with('title', '')
			->with('content', '');
		}		

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user_id;
		$picture_id;

		$title = Request::input('title');
		$content = Request::input('content');
			
		// Validation of new post data
		$validator = Post::validate(Request::all());

		if ($validator->passes()){

			//Validation successfull
			if (Auth::check()){
				$user = Auth::user();
				$user_id = $user->id;
			}
			else
				$user_id = 1; // Author is annonymous

				// Check if picture was uploaded correctly
				if( Request::input('picture') != '' )
					$picture_id = Request::input('picture');
				else
					$picture_id = 'null';

				// Saves post
				$post = Post::create(array(
					'title'		=> $title,
					'content' 	=> $content,
					'user_id' 	=> $user_id,
					'picture_id'=> $picture_id
				));

				// Saves post categories
				if (Request::input('categories') != null)
					$post->categories()->sync(Request::input('categories'));

				// Redirects to blog
				return redirect()->route('post.index')
				->with('message', 'Your post has been created!');
	
		} 
		else{
			// Validation failed
			return redirect()->route('post.index')
			->withErrors($validator)
			->with('title', $title)
			->with('content', $content);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::find($id);

		if (Auth::check()){

			$user = Auth::user();

			if($user->rol == 0){
				// User
				return view('user.post')
				->with('user', $user)
				->with('post', $post);
			}
			else{
				// Administrator
				return view('administrator.post')
				->with('user', $user)
				->with('post', $post);
			}	
		}
		else{
			// Visiter
			return view('visiter.post')
				->with('post', $post);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Tags Post
		$post = Post::find($id);
		
		if (Request::input('tags') != null){
			$post->tags()->sync(Request::input('tags'));
		}
		
		// Redirects to blog - administrator view
		return redirect()->route('administrator.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id);
		$post->delete();

		// Redirects to blog - administrator view
		return redirect()->route('administrator.index');
	}

}
