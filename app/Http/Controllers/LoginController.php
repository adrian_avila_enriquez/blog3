<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use App\User;
use App\Post;
use App\Category;
use App\Tag;
use Auth;
use Session;
use Hash;

class LoginController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Login Form
		Auth::logout();
		
		return view('login');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// User data
		$credentials = array(
			'email'     => Request::input('email'),
			'password'  => Request::input('password')
			); 	

    	// Authentication
		if (Auth::attempt($credentials)) {
			
        	// Auth successfull
			$user = Auth::user();
			
			if($user->rol == 0){		

				return redirect()->route('post.index');

			} else {

				return redirect()->route('administrator.index');
			}
			
		} else {        
        	// Auth failed
			return redirect()->route('login.index');

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	// Shows signup form
	public function signup(){

		return view('signup')
			->with('name', '')
			->with('username', '')
			->with('email', '');
	}

	// Saves new user
	public function doSignup(){

		// Validation of new user data
		$validator = User::validate(Request::all());

		$name = Request::input('name');
		$username = Request::input('username');
		$email = Request::input('email');

		if ($validator->passes()){
			
			// Validation successfull
			User::create(array(
                'name' 		=> $name,
                'username' 	=> $username,
                'email' 	=> $email,
                'password' 	=> Hash::make(Request::input('password'))
        	));

			return redirect()->route('login.index')
				->with('message', 'Your account has been created!');
		}
		else{		

			// Validation failed
			return view('signup')
				->withErrors($validator)
				->with('name', $name)
				->with('username', $username)
				->with('email', $email);
		}
	}

}
