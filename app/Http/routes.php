<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



// Public Routes
Route::get('/', array('uses' => 'PostController@index'));

Route::get('home', 'WelcomeController@index');

Route::resource('login', 'LoginController');

Route::resource('post', 'PostController');

Route::get('signup', array('uses' => 'LoginController@signup', 'as' => 'signup'));

Route::post('signup', array('uses' => 'LoginController@doSignup', 'as' => 'signup'));

Route::post('image', array('uses' => 'ImageController@postImage'));


// Protected Routes
Route::group(['before' => 'auth'], function()
{		
	Route::resource('user', 'UserController');

	Route::resource('category', 'CategoryController');

	Route::resource('tag', 'TagController');

	Route::resource('administrator', 'AdministratorController');

});
