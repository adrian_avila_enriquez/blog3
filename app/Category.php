<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	protected $fillable = array('name');

	protected $dates = ['deleted_at'];

    public function posts(){

      	return $this->belongsToMany('App\Post', 'category_post', 'category_id', 'post_id');    								
    }	

}
