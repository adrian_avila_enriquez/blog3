<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Validator;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
	use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = array('rol', 'name', 'username', 'email', 'password');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    
    //Implement soft deletes <----

	
	public function posts(){

		return $this->hasMany('App\Post');
	}

	public static function validate($input) {

		$rules = array(
			'name'						=> 'Required|Min:3|Max:80',
			'email'     				=> 'Required|Between:3,64|Email|Unique:users',
        	'password'  				=> 'Required|AlphaNum|Between:8,24|confirmed', 
        	'password_confirmation'		=> 'Required|AlphaNum|Between:8,24'
       		);

		return Validator::make($input, $rules);       
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
    * Get the e-mail address where password reminders are sent.
    *
    * @return string
    */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken() {
		return $this->remember_token;
	}


	public function setRememberToken($value) {
		$this->remember_token = $value;
	}


	public function getRememberTokenName() {
		return 'remember_token';
	}
}
