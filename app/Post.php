<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Post extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';

	protected $fillable = array('user_id', 'title', 'content', 'picture_id');

    protected $dates = ['deleted_at'];

	public static function validate($input) {
                
    	$rules = array(
        	        'title'			=> 'Required|Min:2|Max:25',
        	        'content'		=> 'Required|Min:2|Max:100'
        );

        return Validator::make($input, $rules);       
    }

	public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function categories(){

      	return $this->belongsToMany('App\Category', 'category_post', 'post_id', 'category_id');
      								
    }

    public function tags(){

      	return $this->belongsToMany('App\Tag', 'post_tag', 'post_id', 'tag_id');
    }

}
